using UnityEngine;
using System.Collections;

public class GugussAnimation : MonoBehaviour {
	
	// Les tranform modifier par l'animation shoot
	// ici "Bip001 R Clavicle" pour le bras droit.
	// et "Bip001 Head" pour le recul sur la tete.
	public Transform[] mixingTransform;
	
	void Start () {
		//Les trois animation sont en Loop
		this.animation.wrapMode = WrapMode.Loop;
		
		//Anim2 (Shoot)
		//On lui specifie les transform sur lesquel elle s'applique
		//car elle ne doit pas s'appliquer sur tout le model pour laisser les autres animations s'occuper du reste (Run ou Idle)
		foreach(Transform t in mixingTransform)
			this.animation["Anim2"].AddMixingTransform( t );
		
		//Le layer de Anim1(Run) et Anim3(Idle) est celui par defaut (0)
		//Comme l'animation shoot doit prendre le pas sur les animations de deplacement
		//on lui specifie un layer superieur.
		this.animation["Anim2"].layer = 1;
		
		//Juste pour le style parcequ'il est moux du genoux on accelere l'anim de shoot
		this.animation["Anim2"].speed = 2;
		
		//Par defaut aucune anim n'est joue donc on lance le Idle au demarage de la scene
		this.animation.Play("Anim3");
	}
	
	// Update is called once per frame
	void Update () {
		
		//Le CrossFade permet d'avoir une transition interpolée entre l'animation en cour et la nouvelle animation
		//A l'appuie sur 'R' on passe sur l'animation Anim1 (Run)
		//Au release de 'R' on passe sur l'animation Anim3 (Idle)
		if(Input.GetKeyDown(KeyCode.R))
			this.animation.CrossFade("Anim1");
		if(Input.GetKeyUp(KeyCode.R))
			this.animation.CrossFade("Anim3");
		
		
		//Meme chose pour le shoot a une exeption pres.
		//Tant que la touche 'S' est enfonce il faut jouer l'animation Shoot (Anim2)
		// donc le warpMode est Loop
		//Lorsque on release la touche 'S', si on fait un Animation.Stop() l'interpolation avec les autre animation ne se fera pas
		// donc on change le warpMode en Once pour que a la fin de la prochaine boucle d'animation l'interpolation se face toute seul.
		if(Input.GetKeyDown(KeyCode.S))
		{
			this.animation["Anim2"].wrapMode = WrapMode.Loop;
			//Le second parametre de CrossFade est fadeLength qui permet de controler la vitesse d'interpolation (par defaut 0.3f)
			// encore une fois sinon il est moux du genoux
			this.animation.CrossFade("Anim2", 0.15f);
		}
		if(Input.GetKeyUp(KeyCode.S))
		{
			this.animation["Anim2"].wrapMode = WrapMode.Once;
		}
	}
}
