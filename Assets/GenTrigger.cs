using UnityEngine;
using System.Collections;

public class GenTrigger : MonoBehaviour {

	public string message;
	
	void OnTriggerEnter (Collider other) 
	{
		other.gameObject.SendMessage (message, SendMessageOptions.DontRequireReceiver);
	}
}