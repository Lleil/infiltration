using UnityEngine;
using System.Collections;

[RequireComponent(typeof(NavMeshAgent))]

public class Cdestination : MonoBehaviour {
	
	private NavMeshAgent dra;
	public Transform[] target;
	private int index;
	public Transform Joueur;
		
	void Start(){
		dra = this.GetComponent<NavMeshAgent>();
	}
	
	void Update () {
		if(FieldOfView.followMe == false)
		{
			dra.SetDestination(target[index].position);
		}
		else if(FieldOfView.followMe == true)
		{
			dra.SetDestination(Joueur.position);	
		}
		
	}
	
	void OnTriggerEnter(Collider touched) {
    	if (touched.name == target[index].name){
				StartCoroutine("PauseWayPoint");
			}
	}
	
	IEnumerator PauseWayPoint(){
		yield return new WaitForSeconds(1.0F);
		
		index++;
		if(index >= target.Length)
			index=0;
	}
	
}
