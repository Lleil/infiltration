using UnityEngine;
using System.Collections;

public class PlayerAnimation : MonoBehaviour {
	
	public Transform[] mixingTransform;
	Animation mAnimation;
		
	// Use this for initialization
	void Awake () {
		this.mAnimation = this.animation;
		foreach(Transform t in this.mixingTransform)
			this.mAnimation["Anim2"].AddMixingTransform(t);
		this.mAnimation["Anim2"].layer = 1;
		this.mAnimation.wrapMode = WrapMode.Loop;
		this.mAnimation.Play ("Anim3");
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.Z))
			this.mAnimation.CrossFade ("Anim1");
		if(Input.GetKeyUp(KeyCode.Z))
			this.mAnimation.CrossFade ("Anim3");	
	
		if(Input.GetKeyDown(KeyCode.A))
			this.mAnimation.Blend ("Anim2", 1f);
		if(Input.GetKeyUp(KeyCode.A))
			this.mAnimation.Blend ("Anim2", 0f);
	}
}
