using UnityEngine;
using System.Collections;

public class FieldOfView : MonoBehaviour
{
    public float MaxDistance;
    public float Angle;
	public static bool followMe = false;

    public Transform Target;

    void Update()
    {
        if (this.Target == null)
            return;

        /// Step 1.
        if (Vector3.Distance(this.transform.position, this.Target.position) <= this.MaxDistance)
        {
            
            /// Step 2.
            Vector3 lDirection = this.Target.position - this.transform.position;
            Vector3 lForward = this.transform.forward;

            if (Vector3.Angle(lDirection, lForward) <= (this.Angle / 2.0f))
            {
                
				Debug.Log("Cone de vision");
				
                /// Step 3.
                RaycastHit lHit;
                if (Physics.Raycast(this.transform.position, (this.Target.position - this.transform.position), out lHit, Mathf.Infinity))
				{
					Debug.Log(lHit.collider.transform.name);
                    if(lHit.collider.gameObject.tag == this.Target.gameObject.tag)
                		{
                    		Debug.Log("VISIBLE");
							followMe = true;
	
                		}
					else
					{
						followMe =false;	
					}
				}
				

            }
        }
    }
}
