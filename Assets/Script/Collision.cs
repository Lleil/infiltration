using UnityEngine;
using System.Collections;

public class Collision : MonoBehaviour {
	
	public string Level;
	bool die =false;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnTriggerEnter(Collider other) 
	{
		if (other.tag == "Player")
		{
			Application.LoadLevel(Level);
		}
	}
	
	void OnGUI()
	{
		if (die)
		{
		GUI.Label(new Rect(600,350,100,100), "Die!");
			if (GUI.Button(new Rect (600, 450, 100,50),"Retry"))
			{
				Application.LoadLevel(Level);
			}
		}
	}
}
