using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour 
{

	CharacterMotor motor;
	bool win = false;
	public string Level2;
	
	void Start()
	{
		motor = this.GetComponent<CharacterMotor>();
	}

	void Win()
	{
		motor.canControl = false;
		win = true;
		Debug.Log("win");
	}
	
	void OnGUI()
	{
		if (win)
		{
			GUI.Label(new Rect(600,350,100,100), "You win !");
			if (GUI.Button(new Rect (600, 450, 100,50),"NEXT"))
			{
				Application.LoadLevel("Level2");
			}
		}
	}
	
}